package main;

import model.Implementations.GuardImpl;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class GuardCreate {
    public static final String DATE_FORMAT = "MM/dd/yyyy";

    public static final Integer ID_GUARD1 = 1;
    public static final Integer ID_GUARD2 = 2;
    public static final Integer ID_GUARD3 = 3;

    public static final String PASS_GUARD1 = "ciao01";
    public static final String PASS_GUARD2 = "asdasd";
    public static final String PASS_GUARD3 = "querty";

    public static final String NAME_GUARD1 = "Oronzo";
    public static final String SURNAME_GUARD1 = "Cantani";
    public static final String TELEPHONE_NUMBER_GUARD1 = "123456789";
    public static final String NAME_GUARD2 = "Emilie";
    public static final String SURNAME_GUARD2 = "Heskey";
    public static final String TELEPHONE_NUMBER_GUARD2 = "123456789";
    public static final String NAME_GUARD3 = "Gennaro";
    public static final String SURNAME_GUARD3 = "Alfieri";
    public static final String TELEPHONE_NUMBER_GUARD3 = "123456789";


    public static List<GuardImpl> inizializeGuard() {

        SimpleDateFormat format = new SimpleDateFormat(DATE_FORMAT);
        Date date = null;
        try {
            date = format.parse("01/01/1980");
        } catch (
                ParseException e) {
            e.printStackTrace();
        }

        List<GuardImpl> listGuard = new ArrayList<>();
        GuardImpl guard1 = new GuardImpl(NAME_GUARD1, SURNAME_GUARD1, date, 1, TELEPHONE_NUMBER_GUARD1, ID_GUARD1, PASS_GUARD1);
        listGuard.add(guard1);
        GuardImpl guard2 = new GuardImpl(NAME_GUARD2, SURNAME_GUARD2, date, 2, TELEPHONE_NUMBER_GUARD2, ID_GUARD2, PASS_GUARD2);
        listGuard.add(guard2);
        GuardImpl guard3 = new GuardImpl(NAME_GUARD3, SURNAME_GUARD3, date, 3, TELEPHONE_NUMBER_GUARD3, ID_GUARD3, PASS_GUARD3);
        listGuard.add(guard3);

        return listGuard;
    }

}
